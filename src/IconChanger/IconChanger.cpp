// IconChanger.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include "UtilIcon.h"

int _tmain(int argc, _TCHAR* argv[])
{
	int success = 0;
	printf("Change icon of file '%s' to new icon '%s'\n", argv[1], argv[2]);

	HANDLE h = BeginUpdateResource(argv[1], FALSE);
	if(h){
		if(!AddIconToRes(h, argv[2], 1, 101)) 
		{ 
			printf("Error changing icon - call failed\n"); 
			success = -1;
		}
		EndUpdateResource(h, FALSE);
	}
	else { 
		printf("Error changing icon - no HANDLE\n");
		success = -1;
	}

	return success;
}

