// MainDlg.cpp : implementation file
//

#include "stdafx.h"
#include "wme_report.h"
#include "MainDlg.h"
#include "FileUploader.h"
#include "Utils.h"
#include "IMapi.h"

// #define LANG_ENGLISH
#define LANG_GERMAN

// CMainDlg dialog

IMPLEMENT_DYNAMIC(CMainDlg, CDialog)

//////////////////////////////////////////////////////////////////////////
CMainDlg::CMainDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMainDlg::IDD, pParent)
{

}

//////////////////////////////////////////////////////////////////////////
CMainDlg::~CMainDlg()
{
}

//////////////////////////////////////////////////////////////////////////
void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DLGICON, m_Icon);
	DDX_Control(pDX, IDC_DESCRIPTION, m_Description);
	DDX_Control(pDX, IDC_CONTENTS, m_ContentsLink);
	DDX_Control(pDX, IDC_EMAIL, m_EmailLink);
}


BEGIN_MESSAGE_MAP(CMainDlg, CDialog)
	ON_STN_CLICKED(IDC_CONTENTS, &CMainDlg::OnContentsClick)
	ON_STN_CLICKED(IDC_EMAIL, &CMainDlg::OnMailClicked)
END_MESSAGE_MAP()


// CMainDlg message handlers

//////////////////////////////////////////////////////////////////////////
BOOL CMainDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_Icon.SetIcon(::LoadIcon(NULL, IDI_ERROR));

#ifdef LANG_ENGLISH
	m_Description.SetWindowText("Wintermute Engine encountered a problem and needs to be closed.\n\nWe have created an error report that you can send to us. This report will help us to diagnose the cause of this error and improve our software.\n\nThis report doesn't contain any personal information.");
#endif
#ifdef LANG_GERMAN
	m_Description.SetWindowText("Wintermute Engine hat ein Problem festgestellt und muss geschlossen werden.\n\nWir haben einen Fehlerreport erstellt, den Sie uns schicken k�nnen. Dieser Report wird uns helfen, das Problem zu analysieren und zu berichtigen.\n\nDieser Report beinhaltet keinerlei personalisierte Informationen.");
#endif

	::MessageBeep(MB_ICONERROR);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

//////////////////////////////////////////////////////////////////////////
void CMainDlg::OnContentsClick()
{
	::ShellExecute(GetSafeHwnd(), "", m_ZipFile, NULL, NULL, SW_NORMAL);
}

//////////////////////////////////////////////////////////////////////////
void CMainDlg::OnMailClicked()
{
	CIMapi mail;

	if (mail.Error() == CIMapi::IMAPI_SUCCESS)
	{
		mail.To("support@rapaki.de");            //  Set recipient name (me)
		mail.Subject(CString("WME crash report: ") + CUtils::GetFName(m_ZipFile));                //  Subject of this email
		mail.Attach(m_ZipFile);               //  Attaching a file

		// Put text of message in body
		mail.Text("Bitte geben Sie weitere Informationen zu dem aufgetretenen Fehler an.");     //  Set body text
		mail.Send(MAPI_DIALOG);                               //  Now send the mail! 
		CDialog::OnOK();
	}
	else
	{
#ifdef LANG_ENGLISH
		MessageBox("Wintermute Engine was unable to send the e-mail.", "Wintermute Engine", MB_ICONERROR);
#endif
#ifdef LANG_GERMAN
		MessageBox("Wintermute Engine konnte die E-Mail nicht senden. Bitte senden Sie die Dateien manuell.", "Wintermute Engine", MB_ICONERROR);
#endif
	}
}

//////////////////////////////////////////////////////////////////////////
bool CMainDlg::UploadFile()
{
	CFileUploader Uploader;
	CWaitCursor wait;
	if(!Uploader.SendFile(m_ZipFile, CUtils::GetFName(m_ZipFile)))
	{
		MessageBox(CString("Wintermute Engine was unable to send the error report.\n\n") + Uploader.m_LastError, "Wintermute Engine", MB_ICONERROR);
		return false;
	}
	else return true;
}

//////////////////////////////////////////////////////////////////////////
void CMainDlg::OnOK()
{
	if(UploadFile()) CDialog::OnOK();
}
